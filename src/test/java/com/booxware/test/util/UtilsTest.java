package com.booxware.test.util;

import org.hamcrest.core.Is;
import org.hamcrest.core.IsEqual;
import org.hamcrest.core.IsNot;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.*;

public class UtilsTest {
    @Test
    public void generateSalt_should_not_produce_same_values_on_consecutive_calls()  {
        String salt1 = Utils.generateSalt();
        String salt2 = Utils.generateSalt();

        assertThat(salt1, is(not(equalTo(salt2))));
    }

    @Test
    public void generateSalt_should_produce_a_salt_with_a_legth_0f_10() {
        String salt = Utils.generateSalt();

        assertThat(salt.length(), is(11));
    }

    @Test
    public void md5_should_produce_a_hash_value_of_the_given_string_with_a_length_of_16() {
        byte[] tests = Utils.md5("test");

        assertThat(tests.length, is(16));
    }

    @Test
    public void md5_should_produce_the_same_value_for_consecutive_calls() {
        byte[] hash1 = Utils.md5("test");
        byte[] hash2 = Utils.md5("test");

        assertThat(hash1, is(equalTo(hash2)));
    }

    @Test
    public void md5_should_produce_a_specific_value_for_given_argument() {

        byte[] encryptedPassword = Utils.md5("test");

        assertThat(encryptedPassword, is(new byte[]{9, -113, 107, -51, 70, 33, -45, 115, -54, -34, 78, -125, 38, 39, -76, -10}));
    }
}