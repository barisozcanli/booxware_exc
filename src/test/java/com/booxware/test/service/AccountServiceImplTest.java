package com.booxware.test.service;

import com.booxware.test.exception.AccountServiceException;
import com.booxware.test.model.Account;
import com.booxware.test.repository.Persistence;
import org.hamcrest.core.IsNot;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceImplTest {

    AccountService accountService;

    @Mock
    Persistence persistence;

    @Before
    public void setup() {
        doNothing().when(persistence).save(isA(Account.class));
        accountService = new AccountServiceImpl(persistence);
    }

    @Test
    public void register_should_create_a_record_when_there_is_no_existing_username() throws Exception {
        // Given
        String username = "validUsername";
        String password = "testPassword";
        String email = "email@email.com";

        Account account = new Account();
        account.setUsername("validUsername");
        account.setSalt("salt");
        account.setEncryptedPassword(new byte[]{-103, 37, 52, -60, -126, -97, -42, -92, 124, 60, 60, 86, -35, 3, 77, 1});

        when(persistence.findByUsername(username)).thenReturn(null, account);

        // When
        Account registeredUser = accountService.register(username, email, password);

        // Then
        assertThat(registeredUser, is(IsNot.not(nullValue())));
        verify(persistence, times(2)).save(any());
    }

    @Test(expected = AccountServiceException.class)
    public void register_should_throw_exception_when_there_is_an_existing_username() throws Exception {
        // Given
        String username = "existingUsername";
        String password = "testPassword";
        String email = "email@email.com";

        when(persistence.findByUsername(username)).thenReturn(new Account());

        // When
        Account registeredUser = accountService.register(username, email, password);
    }

    @Test
    public void login_should_return_registered_user_when_correct_username_password_is_provided() {
        // Given
        String username = "validUsername";
        String password = "test";

        Account account = new Account();
        account.setUsername("validUsername");
        account.setSalt("salt");
        account.setEncryptedPassword(new byte[]{49, 82, 64, -58, 18, 24, -92, -88, 97, -20, -108, -111, 102, -88, 94, -16});

        when(persistence.findByUsername("validUsername")).thenReturn(account);

        // When
        Account loggedinUser = accountService.login(username, password);

        // Then
        assertThat(loggedinUser.getUsername(), is("validUsername"));
    }

    @Test(expected = AccountServiceException.class)
    public void login_should_throw_exception_when_given_password_is_invalid() throws Exception {
        // Given
        String username = "validUsername";
        String password = "invalidPassword";

        Account account = new Account();
        account.setUsername("validUsername");
        account.setSalt("salt");
        account.setEncryptedPassword(new byte[]{49, 82, 64, -58, 18, 24, -92, -88, 97, -20, -108, -111, 102, -88, 94, -16});

        when(persistence.findByUsername("validUsername")).thenReturn(account);

        // When
        Account loggedinUser = accountService.login(username, password);
    }

    @Test(expected = AccountServiceException.class)
    public void login_should_throw_exception_when_given_username_is_invalid() throws Exception {
        // Given
        String username = "invalidUsername";
        String password = "test";

        when(persistence.findByUsername("invalidUsername")).thenReturn(null);

        // When
        Account loggedinUser = accountService.login(username, password);
    }

    @Test
    public void delete_should_find_user_and_delete_it() {
        // Given
        String username = "username";
        Account account = new Account();
        when(persistence.findByUsername(username)).thenReturn(account);
        doNothing().when(persistence).delete(account);

        // When
        accountService.deleteAccount("username");

        // Then
        verify(persistence, times(1)).findByUsername(username);
        verify(persistence, times(1)).delete(account);
    }

    @Test(expected = AccountServiceException.class)
    public void delete_should_throw_exception_when_user_not_found() throws Exception {
        // Given
        String username = "invalidUsername";
        when(persistence.findByUsername(username)).thenReturn(null);

        // When
        accountService.deleteAccount(username);
    }

    @Test
    public void hasLoggedInSince_should_return_true_when_user_is_logged_in_after_given_date() throws ParseException {
        // Given
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        String loginDateStr = "31-08-2017 10:20:56";
        String checkDateStr = "31-08-2017 09:20:56";
        Date loginDate = sdf.parse(loginDateStr);
        Date checkDate = sdf.parse(checkDateStr);

        String username = "username";
        Account account = new Account();
        account.setLastLogin(loginDate);

        when(persistence.findByUsername(username)).thenReturn(account);

        // When
        boolean hasLoggedIn = accountService.hasLoggedInSince(username, checkDate);

        // Then
        assertThat(hasLoggedIn, is(true));
    }

    @Test
    public void hasLoggedInSince_should_return_false_when_user_is_logged_in_before_given_date() throws ParseException {
        // Given
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        String loginDateStr = "31-08-2017 10:20:56";
        String checkDateStr = "31-08-2017 11:20:56";
        Date loginDate = sdf.parse(loginDateStr);
        Date checkDate = sdf.parse(checkDateStr);

        String username = "username";
        Account account = new Account();
        account.setLastLogin(loginDate);

        when(persistence.findByUsername(username)).thenReturn(account);

        // When
        boolean hasLoggedIn = accountService.hasLoggedInSince(username, checkDate);

        // Then
        assertThat(hasLoggedIn, is(false));
    }

    @Test
    public void hasLoggedInSince_should_return_true_when_user_is_logged_in_at_given_date() throws ParseException {
        // Given
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        String loginDateStr = "31-08-2017 10:20:56";
        String checkDateStr = "31-08-2017 10:20:56";
        Date loginDate = sdf.parse(loginDateStr);
        Date checkDate = sdf.parse(checkDateStr);

        String username = "username";
        Account account = new Account();
        account.setLastLogin(loginDate);

        when(persistence.findByUsername(username)).thenReturn(account);

        // When
        boolean hasLoggedIn = accountService.hasLoggedInSince(username, checkDate);

        // Then
        assertThat(hasLoggedIn, is(true));
    }

    @Test(expected = AccountServiceException.class)
    public void hasLoggedInSince_should_throw_exception_when_user_not_found() {
        // Given
        String username = "invalidUsername";
        when(persistence.findByUsername(username)).thenReturn(null);

        // When
        accountService.hasLoggedInSince(username, new Date());

    }
}