package com.booxware.test.repository;

import com.booxware.test.exception.AccountServiceException;
import com.booxware.test.model.Account;
import com.booxware.test.service.AccountServiceImpl;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doNothing;

public class PersistenceImplTest {

    Persistence persistence;

    Map<String, List<Account>> dataStore = new HashMap<>();

    @Before
    public void setup() {
        List<Account> accounts = new ArrayList<>();
        dataStore.put("account", accounts);
        persistence = new PersistenceImpl(dataStore);
    }

    @Test
    public void save_should_create_a_new_record_if_the_account_has_no_id() {
        // Given
        Account account = new Account();
        account.setUsername("username");
        account.setSalt("salt");
        account.setEmail("email@email.com");
        account.setEncryptedPassword(new byte[]{});
        account.setLastLogin(new Date());

        // When
        persistence.save(account);

        // Then
        assertThat(dataStore.get("account").size(), is(1));
        assertThat(dataStore.get("account").get(0).getUsername(), is("username"));
        assertThat(dataStore.get("account").get(0).getId(), is(1));
    }

    @Test
    public void save_should_update_a_record_if_account_has_an_id() {

        // Given
        Account account = new Account();
        account.setUsername("username");
        account.setId(1);
        account.setSalt("salt");
        account.setEmail("email@email.com");
        account.setEncryptedPassword(new byte[]{});
        account.setLastLogin(new Date());

        dataStore.get("account").add(account);

        // When
        account.setUsername("modifiedUsername");
        persistence.save(account);

        // Then
        assertThat(dataStore.get("account").get(0).getId(), is(1));
        assertThat(dataStore.get("account").get(0).getUsername(), is("modifiedUsername"));
    }

    @Test
    public void findById_should_return_an_account_when_a_valid_id_is_given() {
        // Given
        Account account = new Account();
        account.setUsername("username");
        account.setId(14);
        dataStore.get("account").add(account);

        // When
        Account myAccount = persistence.findById(14);

        // Then
        assertThat(myAccount.getUsername(), is("username"));
        assertThat(myAccount.getId(), is(14));
    }

    @Test(expected = RuntimeException.class)
    public void findById_should_throw_exception_when_there_is_more_than_one_record() {
        // Given
        Account account = new Account();
        account.setUsername("username");
        account.setId(14);
        dataStore.get("account").add(account);

        Account account2 = new Account();
        account2.setUsername("username2");
        account2.setId(14);
        dataStore.get("account").add(account2);

        // When
        Account myAccount = persistence.findById(14);
    }

    @Test
    public void findById_should_return_null_when_there_is_no_record() {
        // When
        Account myAccount = persistence.findById(14);

        assertTrue(myAccount == null);
    }

    @Test(expected = RuntimeException.class)
    public void findByUsername_should_throw_exception_when_there_is_more_than_one_record() {
        // Given
        Account account = new Account();
        account.setUsername("username");
        account.setId(14);
        dataStore.get("account").add(account);

        Account account2 = new Account();
        account2.setUsername("username");
        account2.setId(15);
        dataStore.get("account").add(account2);

        // When
        Account myAccount = persistence.findByUsername("username");
    }

    @Test
    public void findByUsername_should_return_an_account_when_a_valid_username_is_given() {
        // Given
        Account account = new Account();
        account.setUsername("username");
        account.setId(14);
        dataStore.get("account").add(account);

        // When
        Account myAccount = persistence.findByUsername("username");

        // Then
        assertThat(myAccount.getUsername(), is("username"));
        assertThat(myAccount.getId(), is(14));
    }

    @Test
    public void findByUsername_should_return_null_when_there_is_no_record() {
        // When
        Account myAccount = persistence.findByUsername("username");

        assertTrue(myAccount == null);
    }

    @Test
    public void delete_should_remove_a_record_when_an_account_is_given() {
        // Given
        Date date = new Date();
        byte[] password = new byte[]{};
        Account account = new Account();
        account.setUsername("username");
        account.setId(14);
        account.setLastLogin(date);
        account.setEmail("email@email.com");
        account.setSalt("salt");
        account.setEncryptedPassword(password);

        Account account2 = new Account();
        account2.setUsername("username");
        account2.setId(14);
        account2.setLastLogin(date);
        account2.setEmail("email@email.com");
        account2.setSalt("salt");
        account2.setEncryptedPassword(password);

        dataStore.get("account").add(account);

        // When
        persistence.delete(account2);

        assertThat(dataStore.get("account").size(), is(0));
    }
}