package com.booxware.test.repository;

import com.booxware.test.model.Account;
import lombok.RequiredArgsConstructor;

import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class PersistenceImpl implements Persistence {

    private final Map<String, List<Account>> dataStore;
    private final String ACCOUNT_TABLE_NAME = "account";

    @Override
    public void save(Account account) {

        List<Account> accounts = getAccountsDataStore();

        if (account.getId() == 0) {
            Integer maxId = 0;
            if (accounts.size() > 0) {
                maxId = accounts.stream()
                        .max(Comparator.comparing(Account::getId))
                        .map(a -> a.getId()).get();
            }

            account.setId(maxId + 1);
            accounts.add(account);
        } else {
            Account existingAccount = accounts.stream()
                    .filter(a -> a.getId() == account.getId())
                    .collect(Collectors.toList()).get(0);

            existingAccount.setLastLogin(account.getLastLogin());
            existingAccount.setSalt(account.getSalt());
            existingAccount.setEncryptedPassword(account.getEncryptedPassword());
            existingAccount.setUsername(account.getUsername());
            existingAccount.setEmail(account.getEmail());
        }
    }

    @Override
    public Account findById(long id) {
        List<Account> accounts = getAccountsDataStore().stream()
                .filter(a -> a.getId() == id)
                .collect(Collectors.toList());

        if (accounts.size() > 1) {
            throw new RuntimeException("data integrity exception. There should be only one record with id : " + id);
        }

        if (accounts.size() == 0) {
            return null;
        }

        return accounts.get(0);
    }

    @Override
    public Account findByUsername(String username) {
        List<Account> accounts = getAccountsDataStore().stream()
                .filter(a -> a.getUsername().equals(username))
                .collect(Collectors.toList());

        if (accounts.size() > 1) {
            throw new RuntimeException("data integrity exception");
        }

        if (accounts.size() == 0) {
            return null;
        }

        return accounts.get(0);
    }

    @Override
    public void delete(Account account) {
        getAccountsDataStore().removeIf(a -> a.equals(account));
    }

    private List<Account> getAccountsDataStore() {
        return dataStore.get(ACCOUNT_TABLE_NAME);
    }
}
