package com.booxware.test.repository;

import com.booxware.test.model.Account;

public interface Persistence {

    void save(Account account);

    Account findById(long id);

    Account findByUsername(String name);

    void delete(Account account);
}