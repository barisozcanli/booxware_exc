package com.booxware.test.service;

import com.booxware.test.exception.AccountServiceException;
import com.booxware.test.model.Account;
import com.booxware.test.repository.Persistence;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Date;

import static com.booxware.test.util.Utils.generateSalt;
import static com.booxware.test.util.Utils.md5;

@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final Persistence persistence;

    @Override
    public Account login(String username, String password) {
        Account account = persistence.findByUsername(username);

        if (account == null) {
            throw new AccountServiceException("username not found");
        }


        if (!Arrays.equals(md5(password + account.getSalt()), account.getEncryptedPassword())) {
            throw new AccountServiceException("invalid password");
        }

        account.setLastLogin(new Date());
        persistence.save(account);

        return account;
    }

    @Override
    public Account register(String username, String email, String password) {
        Account existingUser = persistence.findByUsername(username);

        if (existingUser != null) {
            throw new AccountServiceException("username already exists");
        }

        Account account = new Account();
        account.setEmail(email);
        account.setUsername(username);
        account.setSalt(generateSalt());
        account.setEncryptedPassword(md5(password + account.getSalt()));
        account.setLastLogin(null);

        persistence.save(account);

        return login(username, password);
    }

    @Override
    public void deleteAccount(String username) {
        Account account = persistence.findByUsername(username);

        if (account == null) {
            throw new AccountServiceException("user not found");
        }

        persistence.delete(account);
    }

    @Override
    public boolean hasLoggedInSince(String username, Date date) {
        Account account = persistence.findByUsername(username);

        if (account == null) {
            throw new AccountServiceException("user not found");
        }

        return account.getLastLogin().compareTo(date) != -1;
    }
}
