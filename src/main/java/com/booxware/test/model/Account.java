package com.booxware.test.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

/**
 * The encryption can be very simple, we don't put much emphasis on the
 * encryption algorithm.
 */

@Getter
@Setter
public final class Account implements Serializable {

    private int id;

    private String username;

    private byte[] encryptedPassword;

    private String salt;

    private String email;

    private Date lastLogin;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (id != account.id) return false;
        if (username != null ? !username.equals(account.username) : account.username != null) return false;
        if (!Arrays.equals(encryptedPassword, account.encryptedPassword)) return false;
        if (salt != null ? !salt.equals(account.salt) : account.salt != null) return false;
        if (email != null ? !email.equals(account.email) : account.email != null) return false;
        return lastLogin != null ? lastLogin.equals(account.lastLogin) : account.lastLogin == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(encryptedPassword);
        result = 31 * result + (salt != null ? salt.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (lastLogin != null ? lastLogin.hashCode() : 0);
        return result;
    }
}
