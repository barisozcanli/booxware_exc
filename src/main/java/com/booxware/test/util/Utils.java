package com.booxware.test.util;

import org.apache.commons.codec.digest.DigestUtils;

import java.security.SecureRandom;
import java.util.Random;

public class Utils {

    public static String generateSalt() {
        final Random r = new SecureRandom();
        byte[] salt = new byte[32];
        r.nextBytes(salt);

        return salt.toString();
    }

    public static byte[] md5(String str) {
        return DigestUtils.md5(str);
    }
}
